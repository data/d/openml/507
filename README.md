# OpenML dataset: space_ga

https://www.openml.org/d/507

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Geographical Analysis Spatial Data

This georeferenced data set was used in:

Pace, R. Kelley, and Ronald Barry, Quick Computation of Regressions with a Spatially
Autoregressive Dependent Variable, Geographical Analysis, Volume 29, Number 3, July
1997, p. 232-247.

It contains 3,107 observations on U.S. county votes cast in the 1980 presidential election.
Specifically, it contains the total number of votes cast in the 1980 presidential election per
county (VOTES), the population in each county of 18 years of age or older (POP), the
population in each county with a 12th grade or higher education (EDUCATION), the
number of owner-occupied housing units (HOUSES), the aggregate income (INCOME), the X
spatial coordinate of the county (XCOORD), and the Y spatial coordinate of the county
(YCOORD).

The dependent variable is the log of the proportion of votes cast for both candidates in the
1980 presidential election. Hence, we can express our dependent variable as ln(VOTES/
POP) = ln(VOTES)-ln(POP).

The overall data set has the following structure

[ln(VOTES/POP) POP EDUCATION HOUSES INCOME XCOORD YCOORD]

Additional details can be found, along with other data, manuscripts, free spatial software, and
so forth, at www.spatial-statistics.com or www.finance.lsu.edu/re (follow the spatial statistics
link). In particular, the above mentioned manuscript which used the data is available for
download. If you have any questions, send an email to kelley@spatial-statistics.com.






Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: 1

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/507) of an [OpenML dataset](https://www.openml.org/d/507). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/507/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/507/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/507/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

